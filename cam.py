# -*- coding: utf-8 -*-

"""
Cam device object. Implements state and functions

MIT License

Copyright (c) 2017 Roman Mindlin

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
"""

import picamera
import RPi.GPIO as gp

import os
import pickle
import numpy as np
from time import sleep

import const

class Cam(object):
    """
    Implements 4 camera's module
    """

    def __init__(self, logger, debug=False):
        ' Init multiplexer module and camera '

        #Init multiplexer
        gp.setmode(gp.BOARD)
        gp.setwarnings(False)
        gp.setup(7, gp.OUT)
        gp.setup(11, gp.OUT)
        gp.setup(12, gp.OUT)

        # Init current camera
        if not debug:
            self.camera = picamera.PiCamera()
            self.select(2)  # 'D'
            self.camera.sensor_mode = 2
            self.camera.exposure_mode = 'auto'
            self.camera.resolution = (640, 480)

            self.shutter = 0
        else:
            self.camera = None

        self._session_num = self.session_num
        self._logger = logger
        self._counter = 0
        self._fcam0 = const.LOCAL_FCAM0

        self._modified = False

        if os.path.isfile(os.path.join(const.LOCAL_PRESETS_PATH, const.LOCAL_PRESET)):
            self.load_params(os.path.join(const.LOCAL_PRESETS_PATH, const.LOCAL_PRESET), debug=debug)
        else:
            # Reset cam parameters
            self._mode = 'auto'
            self._resolution = 2

            self._wb = 'auto'
            self._wb_red = 10.0
            self._wb_blue = 10.0

            self._iso = 0
            self._exp = -1
            self._contr = -2
            self._drc = 'high'

            self._zoom = 100
            self._tilt = 0

            self._ftp = False
            self._hg = False
            self._dp = False

            self.save_params(os.path.join(const.LOCAL_PRESETS_PATH, const.LOCAL_PRESET))


    def select(self, num):
        ' Set working cam '

        assert(1 <= num <= 4)
        # if num == 2:
        #     self._curcam = 2
        #     gp.output(7, False)
        #     gp.output(12, True)
        #     gp.output(11, False)
        #     self.camera.rotation = const.LOCAL_CAMD_ROT
        #
        # elif num == 3:
        #     self._curcam = 3
        #     gp.output(7, False)
        #     gp.output(12, False)
        #     gp.output(11, True)
        #     self.camera.rotation = const.LOCAL_CAMC_ROT
        #
        # elif num == 4:
        #     self._curcam = 4
        #     gp.output(7, True)
        #     gp.output(12, False)
        #     gp.output(11, True)
        #     self.camera.rotation = const.LOCAL_CAMR_ROT
        #
        # else:
        #     self._curcam = 1
        #     gp.output(7, True)
        #     gp.output(12, True)
        #     gp.output(11, False)
        #     self.camera.rotation = const.LOCAL_CAML_ROT

        if num == 4:
            self._curcam = 4
            gp.output(7, True)
            gp.output(12, True)
            gp.output(11, False)
            self.camera.rotation = const.LOCAL_CAMD_ROT

        elif num == 2:
            self._curcam = 2
            gp.output(7, False)
            gp.output(12, False)
            gp.output(11, True)
            self.camera.rotation = const.LOCAL_CAMC_ROT

        elif num == 1:
            self._curcam = 1
            gp.output(7, True)
            gp.output(12, False)
            gp.output(11, True)
            self.camera.rotation = const.LOCAL_CAMR_ROT

        else:
            self._curcam = 3
            gp.output(7, False)
            gp.output(12, True)
            gp.output(11, False)
            self.camera.rotation = const.LOCAL_CAML_ROT

    @property
    def session_num(self):
        ' Returns session number '
        assert(const.LOCAL_SESSION_NUM_FILENAME)
        if hasattr(self, '_session_num'):
            return self._session_num
        else:
            try:
                with open(const.LOCAL_SESSION_NUM_FILENAME, 'r+b') as f:
                    s = pickle.load(f)
                    f.seek(0)
                    pickle.dump(s+1, f)
                    return s
            except:
                try:
                    with open(const.LOCAL_SESSION_NUM_FILENAME, 'wb') as f:
                        pickle.dump(0,f)
                except:
                    self._logger.warning('Session file creation failed!')
                finally:
                    return 0

    @property
    def current_cam(self):
        ' Getter returns current camera number '
        assert((1 <= self._curcam <= 4) and isinstance(self._counter, int))
        return self._curcam

    @property
    def counter(self):
        ' Picture counter getter '
        assert(isinstance(self._counter, int))
        self._counter += 1
        return self._counter

    @property
    def drc(self):
        ' Frame rate value getter '
        return self._drc

    @drc.setter
    def drc(self, value):
        ' Frame rate value setter '
        self.camera.drc_strength = value
        self._drc = self.camera.drc_strength
        self._modified = True

    @property
    def iso(self):
        ' ISO value getter '
        return self._iso

    @iso.setter
    def iso(self, value):
        ' ISO value setter '
        self.camera.iso = value
        self._iso = self.camera.iso
        self._modified = True

    @property
    def exp(self):
        ' EXP value getter '
        return self._exp

    @exp.setter
    def exp(self, value):
        ' EXP value setter '
        self.camera.exposure_compensation = value
        self._exp = self.camera.exposure_compensation
        self._modified = True

    @property
    def contr(self):
        ' Contr value getter '
        return self._contr

    @contr.setter
    def contr(self, value):
        ' Contr value setter '
        self.camera.contrast = value
        self._contr = self.camera.contrast
        self._modified = True

    @property
    def shutter(self):
        ' Shutter value getter '
        return self._shutter

    @shutter.setter
    def shutter(self, value):
        ' Shutter value setter '
        self.camera.shutter_speed = value
        sleep(0.5)
        self._shutter = self.camera.exposure_speed
        self._modified = True

    @property
    def mode(self):
        ' Mode value getter '
        return self._mode

    @mode.setter
    def mode(self, value):
        ' Mode value setter '
        self._mode = value
        if value == 'auto':
            self.iso = 0
            self.shutter = 0
        self._modified = True

    @property
    def resolution(self):
        ' Resolution value getter '
        return self._resolution

    @resolution.setter
    def resolution(self, value):
        ' Resolution value setter '
        self._resolution = int(value)
        self._modified = True

    @property
    def wb(self):
        ' WB value getter '
        return self._wb

    @wb.setter
    def wb(self, value):
        ' WB value setter '
        self.camera.awb_mode = value
        self._wb = self.camera.awb_mode
        self._modified = True

    @property
    def wb_red(self):
        ' WB red value getter '
        return self._wb_red

    @wb_red.setter
    def wb_red(self, value):
        ' WB red value setter '
        self._wb_red = float(value)
        self.camera.awb_gains = (self._wb_red / 10, self._wb_blue / 10)
        self._modified = True

    @property
    def wb_blue(self):
        ' WB blue value getter '
        return self._wb_blue

    @wb_blue.setter
    def wb_blue(self, value):
        ' WB blue value setter '
        self._wb_blue = float(value)
        self.camera.awb_gains = (self._wb_red / 10, self._wb_blue / 10)
        self._modified = True

    @property
    def ftp(self):
        ' FTP switch getter '
        return self._ftp

    @ftp.setter
    def ftp(self, value):
        ' FTP switch setter '
        self._ftp = value == 'true'
        self._modified = True

    @property
    def hg(self):
        ' Hugin switch getter '
        return self._hg

    @hg.setter
    def hg(self, value):
        ' Hugin switch setter '
        self._hg = value == 'true'
        self._modified = True

    @property
    def dp(self):
        ' DP switch getter '
        return self._dp

    @dp.setter
    def dp(self, value):
        ' DP switch setter '
        self._dp = value == 'true'
        self._modified = True

    @property
    def zoom(self):
        ' Zoom value getter '
        return self._zoom

    @zoom.setter
    def zoom(self, value):
        ' Zoom value setter '
        self._zoom = float(value)
        K, D, Knew, x, y, w, h = self.get_video_params()
        if self.camera.rotation in [90, 270]:
            self.camera.zoom = (y, x, h, w) # camera is sidewise
        else:
            self.camera.zoom = (x, y, w, h)
        self._modified = True

    @property
    def tilt(self):
        ' Tilt value getter '
        return self._tilt

    @tilt.setter
    def tilt(self, value):
        ' Tilt value setter '
        self._tilt = int(value)
        K, D, Knew, x, y, w, h = self.get_video_params()
        if self.camera.rotation in [90, 270]:
            self.camera.zoom = (y, x, h, w) # camera is sidewise
        else:
            self.camera.zoom = (x, y, w, h)
        self._modified = True

    def get_video_params(self):
        ' Calculates video conversion matrices '

        cx = 320 - 6 * self._tilt * (1 - self._zoom / 100)
        cy = 240

        fcam = 100 * self._fcam0 / (self._zoom + 0)
        fsc = 0.85 + (100 - self._zoom) / (500 - 2 * self._zoom)
        K = np.array([[fcam, 0., cx],
                      [0., fcam, cy],
                      [0., 0., 1.]])

        D = np.array([0., 0., 0., 0.])

        Knew = K.copy()
        Knew[(0, 1), (0, 1)] = fsc * Knew[(0, 1), (0, 1)]

        x = (1 - self._zoom / 100) / 2 + self._tilt * (1 - self._zoom / 100) / 100
        y = (1 - self._zoom / 100) / 2
        w = self._zoom / 100
        h = self._zoom / 100

        return K, D, Knew, x, y, w, h

    @property
    def is_modified(self):
        ' Was preset modified or not '
        return self._modified

    def get_params(self):
        ' Sends parameters '

        return {'mode': self._mode,
                'resolution': self._resolution,

                'wb': self._wb,
                'wb_red': self._wb_red,
                'wb_blue': self._wb_blue,

                'iso': self._iso,
                'exp': self._exp,
                'contr': self._contr,
                'drc': self._drc,

                'shutter': self.shutter,

                'zoom': self._zoom,
                'tilt': self._tilt,

                'ftp': self._ftp,
                'dp': self._dp,
                'hg': self._hg}


    def set_params(self, params, debug=False):
        ' Sets parameters '

        self._mode = params['mode']
        self._resolution = params['resolution']

        self._wb = params['wb']
        self._wb_red = params['wb_red']
        self._wb_blue = params['wb_blue']

        self._iso = params['iso']
        self._exp = params['exp']
        self._contr = params['contr']
        self._drc = params['drc']

        if not debug:
            self.shutter = params['shutter']

        self._zoom = params['zoom']
        self._tilt = params['tilt']

        self._ftp = params['ftp']
        self._dp = params['dp']
        self._hg = params['hg']


    def load_params(self, name, debug=False):
        ' Loads parameters from file '
        try:
            with open(name, 'rb') as f:
                params = pickle.load(f)
            self.set_params(params, debug=debug)
        except:
            self._logger.warning('Preset "{}" loading failed!'.format(name))


    def save_params(self, name):
        ' Saves parameters from file '
        params = self.get_params()
        try:
            with open(name, 'wb') as f:
                pickle.dump(params, f)
        except:
            self._logger.warning('Preset "{}" file creation failed!'.format(name))


    def cleanup(self):
        ' Cleans up GPIO and picamera objects '
        self.camera.close()
        gp.cleanup()

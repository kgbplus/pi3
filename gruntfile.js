module.exports = function(grunt) {
    grunt.initConfig({
        clean: {
            src: ['templates/all.js']
        },

        concat: {
            'templates/all.js': ['js/buttons.js', 'js/inputs.js', 'js/modals.js', 'js/others.js', 'js/forge-sha256.js']
        },

        uglify: {
          options: {
            // the banner is inserted at the top of the output
            banner: '/*! scripts/all.js <%= grunt.template.today("dd-mm-yyyy") %> */\n'
          },
          dist: {
            files: {
              'templates/all.min.js': ['templates/all.js']
            }
          }
        },

        qunit: {
          files: ['test/**/*.html']
        },

        jshint: {
          // define the files to lint
          files: ['Gruntfile.js', 'js/*.js', 'test/**/*.js'],
          // configure JSHint (documented at http://www.jshint.com/docs/)
          options: {
            // more options here if you want to override JSHint defaults
            globals: {
              jQuery: true,
              console: true,
              module: true
            }
          }
        }

    });

    grunt.loadNpmTasks('grunt-contrib-clean');
    grunt.loadNpmTasks('grunt-contrib-concat');
    // grunt.loadNpmTasks('grunt-contrib-uglify');
    // grunt.loadNpmTasks('grunt-contrib-jshint');
    // grunt.loadNpmTasks('grunt-contrib-qunit');

    grunt.registerTask('default', ['clean', 'concat']);
};

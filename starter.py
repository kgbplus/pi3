# -*- coding: utf-8 -*-

"""
Camera device web interface

MIT License

Copyright (c) 2017 Roman Mindlin

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
"""


import os
from datetime import datetime
from subprocess import run
from string import Template

import RPi.GPIO as GPIO

import const

PWR_LED = 24
SETUP_LED = 23
BUT = 18

if __name__ == "__main__":

    GPIO.setmode(GPIO.BCM)
    GPIO.setwarnings(False)

    GPIO.setup(BUT, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)
    GPIO.setup(PWR_LED, GPIO.OUT)
    GPIO.setup(SETUP_LED, GPIO.OUT)

    GPIO.output(PWR_LED, GPIO.HIGH)

    start_time = datetime.now()
    safe_mode = True
    while ((datetime.now() - start_time).total_seconds() <= 0.1):  # BUTTON CONNECTION WAIT TIME
        if (GPIO.input(BUT)):
            safe_mode = False
            break

    if safe_mode:
        GPIO.output(SETUP_LED, GPIO.HIGH)
        run("ifconfig wlan0 10.1.1.1 netmask 255.255.0.0", shell=True)
        run("killall wpa_supplicant", shell=True)
        run("service dnsmasq start", shell=True)
        run("service hostapd start", shell=True)
    else:
        try:
            if os.path.isfile(os.path.join(const.LOCAL_CONFIG_PATH, 'need_reconfigure')):
                os.remove(os.path.join(const.LOCAL_CONFIG_PATH, 'need_reconfigure'))

                if const.NETWORK_TYPE == 'wired':
                    config = os.path.join('/etc', 'dhcpcd.conf')
                    template = os.path.join(const.LOCAL_CONFIG_PATH, 'dhcpcd_template.conf')

                    cidr = sum([bin(int(x)).count("1") for x in const.NETWORK_MASK.split(".")])
                    fields = {}
                    fields['address'] = const.NETWORK_ADDRESS + '/' + str(cidr)
                    fields['gateway'] = const.NETWORK_GATEWAY
                    fields['dns'] = const.NETWORK_DNS

                    with open(config, 'w') as f, open(template, 'r') as t:
                        config_file = Template(t.read()).substitute(**fields)
                        f.write(config_file)

                    # blank wifi config

                    config = os.path.join('/etc/wpa_supplicant', 'wpa_supplicant.conf')
                    template = os.path.join(const.LOCAL_CONFIG_PATH, 'wpa_supplicant_blank.conf')

                    with open(config, 'w') as f, open(template, 'r') as t:
                        f.write(t.read())

                elif const.NETWORK_TYPE == 'wifi':
                    config = os.path.join('/etc/wpa_supplicant', 'wpa_supplicant.conf')
                    template = os.path.join(const.LOCAL_CONFIG_PATH, 'wpa_supplicant_template.conf')

                    fields = {}
                    fields['ssid'] = const.NETWORK_SSID
                    fields['wifipass'] = const.NETWORK_WIFIPASS

                    with open(config, 'w') as f, open(template, 'r') as t:
                        config_file = Template(t.read()).substitute(**fields)
                        f.write(config_file)

                run("reboot", shell=True)
        except Exception as e:
            print('CRITICAL: Exception while reconfigure! ({})'.format(str(e)))

$("#settings").on('click', function (ev) {
    ev.preventDefault();
    $.get('api/settings')
        .done(function (data) {
            $('#ftpHost').val(data['FTP_HOST']);
            $('#ftpPath').val(data['FTP_PATH']);
            $('#ftpUser').val(data['FTP_USER']);
            $('#ftpFiles').val(data['FTP_FILES']);
            $('#huginTemplate').val(data['template']);
        });
    $('#settings-modal').modal('show');
    return false;
});
$('#settings-modal').on('show.bs.modal', function (e) {
    var sel = $('#save-settings-btn');
    sel.off('click');
    sel.on('click', function (ev) {
        ev.preventDefault();
        $('#settings-modal').modal('hide');
        var data = {val: {{ rnd }}};
        data['ftpHost'] = $('#ftpHost').val();
        data['ftpPath'] = $('#ftpPath').val();
        data['ftpUser'] = $('#ftpUser').val();
        data['ftpPass'] = $('#ftpPass').val();
        data['ftpFiles'] = $('#ftpFiles').is(":checked");
        data['template'] = $('#huginTemplate').val();
        if ($('#passwordField').val().length) {
            data['secret'] = forge_sha256($('#passwordField').val());
        } else {
            data['secret'] = '';
        };

        $.post('api/settings', data)
            .done(function () {
                show_mesg("Success", "success");
            })
            .fail(function () {
                show_mesg("Connection lost!", "danger");
            });
        return false;
    });
});


$("#presets").on('click', function (ev) {
    ev.preventDefault();
    $('#load-preset-btn').prop("disabled", true );
    $('#preset-select').prop("disabled", true );
    $.get('api/list_presets')
        .done(function (data) {
            if(data.length) {
                $('#preset-select').children().remove()
                $.each(data, function (key, value) {
                    if (value.startsWith('+')) {
                        var option = value.slice(1);
                    } else {
                        var option = value;
                    };
                    $('#preset-select')
                        .append($("<option></option>")
                            .attr("value", option)
                            .text(value));
                });
                $('#load-preset-btn').prop("disabled", false );
                $('#preset-select').prop("disabled", false );
            };
            $('#presets-modal').modal('show');
        })
        .fail(function(){
            show_mesg("Connection lost! No presets available", "danger");
        });
    return false;
});
$('#presets-modal').on('show.bs.modal', function (e) {
    var sel = $('#load-preset-btn');
    sel.off('click');
    sel.on('click', function (ev) {
        ev.preventDefault();
        var name = $('#preset-select').val();
        $.get('api/load_preset/' + name)
            .done(function () {
                $('#preset-name-warn').remove();
                $('<div class="text-success" id="preset-name-warn">Loaded</div>').insertAfter('#load-preset-btn');
                get_params();
                $.get('api/list_presets')
                        .done(function (data) {
                            if(data.length) {
                                $('#preset-select').children().remove()
                                $.each(data, function (key, value) {
                                    if (value.startsWith('+')) {
                                        var option = value.slice(1);
                                    } else {
                                        var option = value;
                                    };
                                    $('#preset-select')
                                        .append($("<option></option>")
                                            .attr("value", option)
                                            .text(value));
                                });
                                $('#preset-select').prop("disabled", false );
                            };
                        });
            })
            .fail(function () {
                $('#preset-name-warn').remove();
                $('<div class="text-danger" id="preset-name-warn">Connection error</div>').insertAfter('#load-preset-btn');
            });
        return false;
    });
    var sel = $('#save-preset-btn');
    sel.off('click');
    sel.on('click', function (ev) {
        ev.preventDefault();
        var name = $('#preset-name');
        if(name.val().length) {
            $('#preset-name-warn').remove();
            var data = {val: {{ rnd }}};
            data['name'] = name.val();
            $.post('api/save_preset', data)
                .done(function () {
                    $('#preset-name-warn').remove();
                    $('<div class="text-success" id="preset-name-warn">Saved</div>').insertAfter('#save-preset-btn');
                    name.val('');
                    $.get('api/list_presets')
                        .done(function (data) {
                            if(data.length) {
                                $('#preset-select').children().remove()
                                $.each(data, function (key, value) {
                                    if (value.startsWith('+')) {
                                        var option = value.slice(1);
                                    } else {
                                        var option = value;
                                    };
                                    $('#preset-select')
                                        .append($("<option></option>")
                                            .attr("value", option)
                                            .text(value));
                                });
                                $('#preset-select').prop("disabled", false );
                            };
                        });
                })
                .fail(function () {
                    $('#preset-name-warn').remove();
                    $('<div class="text-danger" id="preset-name-warn">Connection error</div>').insertAfter('#save-preset-btn');
                });
        } else {
            $('<div class="text-warning" id="preset-name-warn">Please enter valid preset name</div>').insertAfter('#save-preset-btn');
        };
        return false;
    });
});
$("#presets-modal").on('hide.bs.modal', function (ev) {
    $('#preset-name-warn').remove();
    $('#preset-name').val('');
});


$("#switchoff").on('click', function (ev) {
    ev.preventDefault();
    $('#switchoff-modal').modal('show');
    return false;
});
$('#switchoff-modal').on('show.bs.modal', function (e) {
    var sel = $('#switchoff-btn');
    sel.off('click');
    sel.on('click', function (ev) {
        ev.preventDefault();
        $('#switchoff-modal').modal('hide');
        $("#mode-input").val("off");
        previewMode = "off";
        $("#preview-img").attr('style', 'filter: grayscale(100%);');
        $.post('api/switchoff', {val: {{ rnd }}})
            .done(function () {
                show_mesg("Success", "success");
            })
            .fail(function () {
                show_mesg("Connection lost!", "danger");
            });
        return false;
    });
    var sel = $('#exit-btn');
    sel.off('click');
    sel.on('click', function (ev) {
        ev.preventDefault();
        $('#switchoff-modal').modal('hide');
        $("#mode-input").val("off");
        previewMode = "off";
        $("#preview-img").attr('style', 'filter: grayscale(100%);');
        $.post('api/shutdown', {val: {{ rnd }}})
            .done(function () {
                show_mesg("Success", "success");
            })
            .fail(function () {
                show_mesg("Connection lost!", "danger");
            });
        return false;
    });
    var sel = $('#restart-btn');
    sel.off('click');
    sel.on('click', function (ev) {
        ev.preventDefault();
        $('#switchoff-modal').modal('hide');
        $("#mode-input").val("off");
        previewMode = "off";
        $("#preview-img").attr('style', 'filter: grayscale(100%);');
        $.post('api/restart', {val: {{ rnd }}})
            .done(function () {
                show_mesg("Success. Reload page after 30 seconds", "success");
            })
            .fail(function () {
                show_mesg("Connection lost!", "danger");
            });
        return false;
    });
});


function checkInterval2() {
    var fromHour = $("#from-hour").val(), toHour = $("#to-hour").val();
    if ($.isNumeric(fromHour) && $.isNumeric(toHour)) {
        fromHour = parseInt(fromHour);
        toHour = parseInt(toHour);
        if (fromHour >= 0 && toHour < 25 && fromHour < toHour && toHour-fromHour != 24) {
            $("#fake-from-hour").val((toHour==24) ? 0 : toHour);
            $("#fake-to-hour").val(fromHour);
            $("#interval2").attr("readonly", false);
            return false;
        }
    }
    $("#fake-from-hour").val('');
    $("#fake-to-hour").val('');
    $("#interval2").val('');
    $("#interval2").attr("readonly", true);
    return false;
};
$("#schedule").on('click', function (ev) {
    ev.preventDefault();
    $.get('api/schedule')
        .done(function (data) {
            $('#from-hour').val(data['SCHEDULER_FROM_HOUR']);
            $('#to-hour').val(data['SCHEDULER_TO_HOUR']);
            $('#interval').val(data['SCHEDULER_INTERVAL']);
            $('#interval2').val(data['SCHEDULER_INTERVAL2']);
            checkInterval2();
            $('#schedule-modal').modal('show');
        })
        .fail(function () {
            show_mesg("Connection lost!", "danger");
        });
    return false;
});
$("#from-hour").on('change', function (ev) {
    ev.preventDefault();
    return checkInterval2();
});
$("#to-hour").on('change', function (ev) {
    ev.preventDefault();
    return checkInterval2();
});
$("#schedule-modal").on('show.bs.modal', function (e) {
    var sel = $('#save-schedule-btn');
    sel.off('click');
    sel.on('click', function (ev) {
        ev.preventDefault();
        $('#schedule-modal').modal('hide');
        var data = {val: {{ rnd }}};
        data['fromHour'] = $('#from-hour').val();
        data['toHour'] = $('#to-hour').val();
        data['interval'] = $('#interval').val();
        data['interval2'] = $('#interval2').val();
        $.post('api/schedule', data)
            .done(function () {
                show_mesg("Success", "success");
            })
            .fail(function () {
                show_mesg("Connection lost!", "danger");
            });
        return false;
    });
});


$("#network").on('click', function (ev) {
    ev.preventDefault();
    $('#network-modal').modal('show');
    $.get('api/network')
        .done(function (data) {
            if (data['NETWORK_TYPE'] == 'wired') {
                $('#network-tab').tab('show');
            } else {
                $('#wifi-tab').tab('show');
            };
            $('#address').val(data['NETWORK_ADDRESS']);
            $('#netmask').val(data['NETWORK_MASK']);
            $('#gateway').val(data['NETWORK_GATEWAY']);
            $('#dns').val(data['NETWORK_DNS']);
            $('#ssid').val(data['NETWORK_SSID']);
            $('#wifipass').val(data['NETWORK_WIFIPASS']);
            $('#network-modal').modal('show');
        })
        .fail(function () {
            show_mesg("Connection lost!", "danger");
        });
    return false;
});
function valid(target) {
    if (target['networkType'] == 'wired') {
        for (var member in target) {
            if (member == 'ssid' || member == 'wifipass')
                continue;
            if (target[member].length == 0)
                return false;
        }
    } else {
        if (target['ssid'].length == 0)
            return false;
    }
    return true;
}
$("#network-modal").on('show.bs.modal', function (e) {
    var sel = $('#save-network-btn');
    sel.off('click');
    sel.on('click', function (ev) {
        ev.preventDefault();
        var data = {val: {{rnd}} };
        data['address'] = $('#address').val();
        data['netmask'] = $('#netmask').val();
        data['gateway'] = $('#gateway').val();
        data['dns'] = $('#dns').val();
        data['ssid'] = $('#ssid').val();
        data['wifipass'] = $('#wifipass').val();
        data['networkType'] = ($('#network-tab').hasClass('active')) ? 'wired' : 'wifi';
        if (valid(data)) {
            $('#network-warn').remove();
            $('#network-modal').modal('hide');
            $.post('api/network', data)
                .done(function () {
                    show_mesg("Success. Please restart device!", "success");
                })
                .fail(function () {
                    show_mesg("Connection lost!", "danger");
                });
        } else {
            $('#network-warn').remove();
            $('<div class="text-danger" id="network-warn">Fill in all required fields!</div>').insertAfter('#wifi-page');
        }
        return false;
    });
    var sel = $('#restart-btn');
    sel.off('click');
    sel.on('click', function (ev) {
        ev.preventDefault();
        $('#switchoff-modal').modal('hide');
        $("#mode-input").val("off");
        previewMode = "off";
        $("#preview-img").attr('style', 'filter: grayscale(100%);');
        $.post('api/restart', {val: {{ rnd }}})
            .done(function () {
                show_mesg("Success. Reload page after 30 seconds", "success");
            })
            .fail(function () {
                show_mesg("Connection lost!", "danger");
            });
        return false;
    });
});

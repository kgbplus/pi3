
QUnit.test("select buttons", function (assert) {

    $.ajax = function (options) {
        options.url("/api/select/1");
        options.success("Ok");
    };

    $("#btn-L").trigger($.Event("click"));

    assert.equal($("#responseFromServer").text(), "Ok");
});

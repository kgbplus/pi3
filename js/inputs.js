$("#mode-input").on('change', function (ev) {
    var sel = $(this);
    ev.preventDefault();
    if (sel.val() == "off") {
        previewMode = "off";
        return;
    }
    sel.attr("disabled", true);
    previewMode = "on";
    $.post('api/mode', {val: sel.val()})
        .done(function (data) {
            sel.attr("disabled", false);
            if (data == "auto") {
                $("#iso-input").val(0);
                $("#wb-input").val("auto");
                $.get('/api/sauto')
                    .done(function (data) {
                        $("#shutter").val(data);
                    })
                    .fail(function () {
                        show_mesg("ERROR! Not connected!", "danger");
                    });
            }
        })
        .fail(function () {
            show_mesg("ERROR! Not connected!", "danger");
            sel.attr("disabled", false);
        });
    updateImg();
    return false;
});

$("#res-input").on('change', function (ev) {
    var sel = $(this);
    sel.attr("disabled", true);
    ev.preventDefault();
    $.post('api/res', {val: sel.val()})
        .done(function () {
            sel.attr("disabled", false);
        })
        .fail(function () {
            show_mesg("ERROR! Not connected!", "danger");
            sel.attr("disabled", false);
        });
    return false;
});

$("#wb-input").on('change', function (ev) {
    var sel = $(this);
    sel.attr("disabled", true);
    ev.preventDefault();
    $.post('api/wb', {val: sel.val()})
        .done(function () {
            sel.attr("disabled", false);
        })
        .fail(function () {
            show_mesg("ERROR! Not connected!", "danger");
            sel.attr("disabled", false);
        });
    return false;
});
$("#wb-red-input").on('change', function (ev) {
    var sel = $(this);
    sel.attr("disabled", true);
    ev.preventDefault();
    $.post('api/wb_red', {val: sel.val()})
        .done(function () {
            sel.attr("disabled", false);
        })
        .fail(function () {
            show_mesg("ERROR! Not connected!", "danger");
            sel.attr("disabled", false);
        });
    return false;
});
$("#wb-blue-input").on('change', function (ev) {
    var sel = $(this);
    sel.attr("disabled", true);
    ev.preventDefault();
    $.post('api/wb_blue', {val: sel.val()})
        .done(function () {
            sel.attr("disabled", false);
        })
        .fail(function () {
            show_mesg("ERROR! Not connected!", "danger");
            sel.attr("disabled", false);
        });
    return false;
});

$("#ftp-input").on('change', function (ev) {
    var sel = $(this);
    sel.attr("disabled", true);
    ev.preventDefault();
    $.post('api/ftp', {val: sel.is(':checked')})
        .done(function () {
            sel.attr("disabled", false);
        })
        .fail(function () {
            show_mesg("ERROR! Not connected!", "danger");
            sel.attr("disabled", false);
        });
    return false;
});
$("#hg-input").on('change', function (ev) {
    var sel = $(this);
    sel.attr("disabled", true);
    ev.preventDefault();
    $.post('api/hg', {val: sel.is(':checked')})
        .done(function () {
            sel.attr("disabled", false);
        })
        .fail(function () {
            show_mesg("ERROR! Not connected!", "danger");
            sel.attr("disabled", false);
        });
    return false;
});
$("#dp-input").on('change', function (ev) {
    var sel = $(this);
    sel.attr("disabled", true);
    ev.preventDefault();
    $.post('api/dp', {val: sel.is(':checked')})
        .done(function () {
            sel.attr("disabled", false);
        })
        .fail(function () {
            show_mesg("ERROR! Not connected!", "danger");
            sel.attr("disabled", false);
        });
    return false;
});

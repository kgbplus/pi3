// $(function () {
var previewMode = "on";
var pano;

$('.ip_address').mask('099.099.099.099');

$('#ex1').slider({
    formatter: function (value) {
        return value;
    }
});
$('#ex2').slider({
    formatter: function (value) {
        return value;
    }
});

function get_params() {
    $.getJSON('api/params')
        .done(function (data) {
            $("#shutter").val("1/"+Math.floor(1e6/data.shutter));
            $('#exp-input').val(data.exp);
            $('#contr-input').val(data.contr);
            $('#wb-blue-input').val(data.wb_blue);
            $('#wb-red-input').val(data.wb_red);
            $('#wb-input').val(data.wb);
            $('#iso-input').val(data.iso);
            $('#drc-input').val(data.drc);
            if (data.ftp) {
                $('#ftp-input').parent().addClass('active');
            }
            if (data.dp) {
                $('#dp-input').parent().addClass('active');
            }
            if (data.hg) {
                $('#hg-input').parent().addClass('active');
            }
            $('#mode-input').val(data.mode);
            $('#res-input').val(data.resolution);
            $("#ex1").slider('setValue', data.zoom);
            $("#ex2").slider('setValue', data.tilt);
        })
        .fail(function () {
            $("#mode-input").val("off");
            previewMode = "off";
            $("#preview-img").attr('style', 'filter: grayscale(100%);');
            show_mesg("ERROR! Not connected!", "danger");
        });
}
get_params();


$("#ex1").on("slideStop", function() {
    var sel = $(this);
    sel.slider('disable');
    $.post('api/zoom', {val: sel.slider('getValue')})
        .done(function () {
            sel.slider('enable');
        })
        .fail(function () {
            show_mesg("ERROR! Not connected!", "danger");
            sel.slider('enable');
        });
});
$("#ex2").on("slideStop", function() {
    var sel = $(this);
    sel.slider('disable');
    $.post('api/tilt', {val: sel.slider('getValue')})
        .done(function () {
            sel.slider('enable');
        })
        .fail(function () {
            show_mesg("ERROR! Not connected!", "danger");
            sel.slider('enable');
        });
});

updateImg();

$("#status").text("Raspberry Pi server: " + location.host);
setInterval(updateStatus, 1000);

function updateImg() {
    if (previewMode == "off") {
        $("#preview-img").attr('style', 'filter: grayscale(100%);');
        return;
    }
    var img = $("<img id=\"preview-img\" class=\"img-fluid text-center\" />").attr("src", "/video_feed?" + new Date().getTime())
        .on('load', function () {
            if (!this.complete || typeof this.naturalWidth == "undefined" || this.naturalWidth == 0) {
                console.log('broken image!');
            } else {
                var pr = $("#preview");
                pr.find("img").remove();
                pr.append(img);
                setTimeout(updateImg, 10)
            }
        })
        .on('error', function() {
            $("#mode-input").val("off");
            $("#preview-img").attr('style', 'filter: grayscale(100%);');
            show_mesg("Connection lost!", "danger");
    });
}


function updateStatus() {
    var d = new Date();

    var month = d.getMonth() + 1;
    var day = d.getDate();
    var date_output = (day < 10 ? '0' : '') + day + '.' + (month < 10 ? '0' : '') + month + '.' + d.getFullYear();

    var hour = d.getHours();
    var minute = d.getMinutes();
    var second = d.getSeconds();

    var time_output = (hour < 10 ? '0' : '') + hour + ":" + (minute < 10 ? '0' : '') + minute + ":" +
        (second < 10 ? '0' : '') + second;

    var line = date_output + "    " + time_output;

    $("#status-time").text(line);
}


function show_mesg(text, cls) {
    $("#mesg").html(
        " <div class=\"alert alert-" + cls + " alert-dismissible show\" role=\"alert\">\n" +
        "            <button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\">\n" +
        "                <span aria-hidden=\"true\">&times;</span>\n" +
        "            </button>\n" +
        text + "\n" +
        "</div>"
    );
    $(".alert").delay(5000).slideUp(400, function () {
        $(this).alert('close');
    });
}


// function sendPano() {
//     var btn = $(":first-child", ("#btn-F4"));
//     btn.addClass('active disabled');
//     $.get('/api/f4')
//         .done(function (data) {
//             show_mesg("Panoramic project " + data + " saved", "success");
//             btn.removeClass('active disabled');
//             var tlInt = parseInt($('#tl-int-input').val());
//             if (tlInt & $("#tl-input").is(':checked')) {
//                 pano = setTimeout(sendPano, tlInt*1000);
//             }
//         })
//         .fail(function (data) {
//             if (data.responseText == "ftp error") {
//                 show_mesg("FTP connection error!", "danger");
//             } else {
//                 show_mesg("ERROR! Panoramic picture creation failed!", "danger");
//             }
//             btn.removeClass('active disabled');
//         });
// }

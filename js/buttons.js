$("#btn-L").on('click', function (ev) {
    var sel = $(this);
    sel.attr("disabled", true);
    ev.preventDefault();
    $.get('/api/select/1')
    .done(function () {
            sel.attr("disabled", false);
        })
    .fail(function () {
        show_mesg("ERROR! Not connected!", "danger");
        sel.attr("disabled", false);
    });
    return false;
});
$("#btn-D").on('click', function (ev) {
    var sel = $(this);
    sel.attr("disabled", true);
    ev.preventDefault();
    $.get('/api/select/2')
    .done(function () {
            sel.attr("disabled", false);
        })
    .fail(function () {
        show_mesg("ERROR! Not connected!", "danger");
        sel.attr("disabled", false);
    });
    return false;
});
$("#btn-C").on('click', function (ev) {
    var sel = $(this);
    sel.attr("disabled", true);
    ev.preventDefault();
    $.get('/api/select/3')
    .done(function () {
            sel.attr("disabled", false);
        })
    .fail(function () {
        show_mesg("ERROR! Not connected!", "danger");
        sel.attr("disabled", false);
    });
    return false;
});
$("#btn-R").on('click', function (ev) {
    var sel = $(this);
    sel.attr("disabled", true);
    ev.preventDefault();
    $.get('/api/select/4')
    .done(function () {
            sel.attr("disabled", false);
        })
    .fail(function () {
        show_mesg("ERROR! Not connected!", "danger");
        sel.attr("disabled", false);
    });
    return false;
});

$("#btn-F1").on('click', function (ev) {
    var btn = $(":first-child", this);
    btn.addClass('active disabled');
    ev.preventDefault();
    $.get('/api/f1')
        .done(function (data) {
            show_mesg("Picture " + data + " saved", "success");
            btn.removeClass('active disabled');
        })
        .fail(function () {
            show_mesg("ERROR! Picture creation failed!", "danger");
            btn.removeClass('active disabled');
        });
    return false;
});
$("#btn-F4").on('click', function (ev) {
    var btn = $(":first-child", this);
    btn.addClass('active disabled');
    ev.preventDefault();
    $.get('/api/f4')
        .done(function (data) {
            show_mesg("Panoramic project " + data + " saved", "success");
            btn.removeClass('active disabled');
        })
        .fail(function (data) {
            if (data.responseText == "ftp error") {
                show_mesg("FTP connection error!", "danger");
            } else {
                show_mesg("ERROR! Panoramic picture creation failed!", "danger");
            }
            btn.removeClass('active disabled');
        });
    return false;
});

$("#btn-LC").on('click', function (ev) {
    var btn = $(":first-child", this);
    btn.addClass('active disabled');
    ev.preventDefault();
    $.get('/api/lc')
        .done(function (data) {
            $("#lc-info").text('LC = ' + data);
            btn.removeClass('active disabled');
        })
        .fail(function () {
            show_mesg("ERROR! Not connected!", "danger");
            btn.removeClass('active disabled');
        });
    return false;
});

$("#iso-input").on('change', function (ev) {
    var sel = $(this);
    sel.attr("disabled", true);
    ev.preventDefault();
    $.post('api/iso', {val: sel.val()})
        .done(function () {
            sel.attr("disabled", false);
            $("#mode-input").val("P");
        })
        .fail(function () {
            show_mesg("ERROR! Not connected!", "danger");
            sel.attr("disabled", false);
        });
    return false;
});
$("#exp-input").on('change', function (ev) {
    var sel = $(this);
    sel.attr("disabled", true);
    ev.preventDefault();
    $.post('api/exp', {val: sel.val()})
        .done(function () {
            sel.attr("disabled", false);
        })
        .fail(function () {
            show_mesg("ERROR! Not connected!", "danger");
            sel.attr("disabled", false);
        });
    return false;
});
$("#contr-input").on('change', function (ev) {
    var sel = $(this);
    sel.attr("disabled", true);
    ev.preventDefault();
    $.post('api/contr', {val: sel.val()})
        .done(function () {
            sel.attr("disabled", false);
        })
        .fail(function () {
            show_mesg("ERROR! Not connected!", "danger");
            sel.attr("disabled", false);
        });
    return false;
});
$("#drc-input").on('change', function (ev) {
    var sel = $(this);
    sel.attr("disabled", true);
    ev.preventDefault();
    $.post('api/drc', {val: sel.val()})
        .done(function () {
            sel.attr("disabled", false);
        })
        .fail(function () {
            show_mesg("ERROR! Not connected!", "danger");
            sel.attr("disabled", false);
        });
    return false;
});

$("#btn-plus").on('click', function (ev) {
    var btn = $(this);
    btn.addClass('active disabled');
    ev.preventDefault();
    $.get('/api/splus')
        .done(function (data) {
            $("#shutter").val(data);
            $("#mode-input").val("P");
            btn.removeClass('active disabled');
        })
        .fail(function () {
            show_mesg("ERROR! Not connected!", "danger");
            btn.removeClass('active disabled');
        });
    return false;
});
$("#btn-minus").on('click', function (ev) {
    var btn = $(this);
    btn.addClass('active disabled');
    ev.preventDefault();
    $.get('/api/sminus')
        .done(function (data) {
            $("#shutter").val(data);
            $("#mode-input").val("P");
            btn.removeClass('active disabled');
        })
        .fail(function () {
            show_mesg("ERROR! Not connected!", "danger");
            btn.removeClass('active disabled');
        });
    return false;
});
$("#btn-sauto").on('click', function (ev) {
    var btn = $(this);
    btn.addClass('active disabled');
    ev.preventDefault();
    $.get('/api/sauto')
        .done(function (data) {
            $("#shutter").val(data);
            btn.removeClass('active disabled');
        })
        .fail(function () {
            show_mesg("ERROR! Not connected!", "danger");
            btn.removeClass('active disabled');
        });
    return false;
});

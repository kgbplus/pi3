<<<<<<< HEAD
# -*- coding: utf-8 -*-

"""
Camera device web interface

MIT License

Copyright (c) 2017 Roman Mindlin

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
"""


import glob
import os
import ftplib
from time import sleep
from threading import Thread, Event

import const
import logger


logger = logger.get_logger('app.py')


class Uploader(object):
    """
    Implements fotopicam uploader module
    """

    def __init__(self, enable=True):
        ' Cleans unnecessary files and starts the uploader '
        files_to_clean = glob.glob(os.path.join(const.LOCAL_PICTURES_PATH, '*.tif'))
        if not const.FTP_FILES:
            files_to_clean.extend(glob.glob(os.path.join(const.LOCAL_PICTURES_PATH, 'c*.jpg')))
            files_to_clean.extend(glob.glob(os.path.join(const.LOCAL_PICTURES_PATH, 'c*.pto')))
        for file in files_to_clean:
            os.remove(file)
        self._fotopicam = glob.glob(os.path.join(const.LOCAL_PICTURES_PATH, '*.jpg'))
        self._fotopicam = sorted(self._fotopicam, key=os.path.getmtime)

        self._con = None
        self._enable = enable
        self._pause = False
        self._stop_event = Event()
        self._t = Thread(target=self.worker).start()

    def worker(self):
        ' Main uploader worker. It sends files one by one until empty queue '
        try:
            while True:
                if self._pause:
                    sleep(30)
                    self._sleep = False
                if self._fotopicam and self._enable: #we have files to send and uploader not paused
                    self._con = ftplib.FTP(const.FTP_HOST) #trying to connect
                    if self._con: #if connected
                        self._con.login(const.FTP_USER, const.FTP_PASS)
                        self._con.cwd(const.FTP_PATH)

                        for file in self._fotopicam:
                            if os.path.getsize(file) > 0:
                                logger.info('Trying {}:'.format(file))
                                with open(file, 'rb') as f:
                                    self._con.storbinary("STOR " + os.path.basename(file), f)
                                    logger.info('FTP uploaded successfully: {}'.format(os.path.basename(file)))
                                    os.remove(file)
                                    self._fotopicam.remove(file)
                                if self._stop_event.is_set():
                                    break

                        self._con.close()

                if self._stop_event.is_set():
                    break
                else:
                    sleep(1)
        except Exception as e:
            if self._con:
                self._con.close()
            logger.error('FTP uploader error: {}'.format(str(e)))
            sleep(5)
            self._t = Thread(target=self.worker).start()

    def append(self, files):
        ' Inserts new files into the uploaders queue'
        if files:
            self._fotopicam.extend([const.LOCAL_PICTURES_PATH + f for f in files])
            self._pause = True

    def enable(self, value):
        ' Pauses and unpauses uploader '
        self._enable = value

    def close(self):
        ' Close uploader '
        self._stop_event.set()
=======
# -*- coding: utf-8 -*-

"""
Camera device web interface

MIT License

Copyright (c) 2017 Roman Mindlin

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
"""


import glob
import os
import ftplib
from time import sleep
from threading import Thread, Event

import const
import logger


logger = logger.get_logger('app.py')


class Uploader(object):
    """
    Implements fotopicam uploader module
    """

    def __init__(self, enable=True):
        ' Cleans unnecessary files and starts the uploader '
        files_to_clean = glob.glob(os.path.join(const.LOCAL_PICTURES_PATH, '*.tif'))
        if not const.FTP_FILES:
            files_to_clean.extend(glob.glob(os.path.join(const.LOCAL_PICTURES_PATH, 'c*.jpg')))
            files_to_clean.extend(glob.glob(os.path.join(const.LOCAL_PICTURES_PATH, 'c*.pto')))
        for file in files_to_clean:
            os.remove(file)
        self._fotopicam = glob.glob(os.path.join(const.LOCAL_PICTURES_PATH, '*.jpg'))
        self._fotopicam = sorted(self._fotopicam, key=os.path.getmtime)

        self._con = None
        self._enable = enable
        self._pause = False
        self._stop_event = Event()
        self._t = Thread(target=self.worker).start()

    def worker(self):
        ' Main uploader worker. It sends files one by one until empty queue '
        try:
            while True:
                if self._pause:
                    sleep(30)
                    self._sleep = False
                if self._fotopicam and self._enable: #we have files to send and uploader not paused
                    self._con = ftplib.FTP(const.FTP_HOST) #trying to connect
                    if self._con: #if connected
                        self._con.login(const.FTP_USER, const.FTP_PASS)
                        self._con.cwd(const.FTP_PATH)

                        for file in self._fotopicam:
                            logger.info('Trying {}:'.format(file))
                            with open(file, 'rb') as f:
                                self._con.storbinary("STOR " + os.path.basename(file), f)
                                logger.info('FTP uploaded successfully: {}'.format(os.path.basename(file)))
                                os.remove(file)
                                self._fotopicam.remove(file)
                            if self._stop_event.is_set():
                                break

                        self._con.close()

                if self._stop_event.is_set():
                    break
                else:
                    sleep(1)
        except Exception as e:
            if self._con:
                self._con.close()
            logger.error('FTP uploader error: {}'.format(str(e)))
            sleep(5)
            self._t = Thread(target=self.worker).start()

    def append(self, files):
        ' Inserts new files into the uploaders queue'
        if files:
            self._fotopicam.extend([const.LOCAL_PICTURES_PATH + f for f in files])
            self._pause = True

    def enable(self, value):
        ' Pauses and unpauses uploader '
        self._enable = value

    def close(self):
        ' Close uploader '
        self._stop_event.set()
>>>>>>> 5eb7f4b278b7bc27273ee8e51cd45920f6b8b79e

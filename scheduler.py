# -*- coding: utf-8 -*-

"""
Cam device object. Implements state and functions

MIT License

Copyright (c) 2017 Roman Mindlin

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
"""

from apscheduler.schedulers.background import BackgroundScheduler

import const
from datetime import datetime


class F4Scheduler(object):
    """
    Implements camera scheduler module
    """

    def __init__(self, proc):
        ' Init scheduler object '
        self._proc = proc
        self._job = None
        self._scheduler = BackgroundScheduler()

    @staticmethod
    def get_interval():
        ' Returns relevant interval value '
        if int(const.SCHEDULER_FROM_HOUR) <= datetime.now().hour < int(const.SCHEDULER_TO_HOUR):
            return int(const.SCHEDULER_INTERVAL)
        else:
            return int(const.SCHEDULER_INTERVAL2)

    @staticmethod
    def get_endofperiod():
        ' Calculates correct end of period hour '
        if int(const.SCHEDULER_FROM_HOUR) <= datetime.now().hour < int(const.SCHEDULER_TO_HOUR):
            return int(const.SCHEDULER_TO_HOUR) if int(const.SCHEDULER_TO_HOUR) != 24 else 0
        else:
            return int(const.SCHEDULER_FROM_HOUR)

    def set(self):
        ' Setup jobs and start scheduler '
        interval = self.get_interval()
        if interval:
            self._job = self._scheduler.add_job(self._proc, trigger='interval', seconds=interval)
        else:
            self._job = None
        self._updater = self._scheduler.add_job(self.update, trigger='cron', hour=self.get_endofperiod())
        self._scheduler.start()

    def update(self):
        ' Update jobs, remove unnecessary jobs. Should be used at the end of period '
        interval = self.get_interval()
        if interval:
            if self._job is not None:
                self._job = self._scheduler.reschedule_job(self._job.id, trigger='interval', seconds=self.get_interval())
            else:
                self._job = self._scheduler.add_job(self._proc, trigger='interval', seconds=interval)
        else:
            if self._job is not None:
                self._scheduler.remove_job(self._job.id)
                self._job = None
        self._updater = self._scheduler.reschedule_job(self._updater.id, trigger='cron', hour=self.get_endofperiod())

    def clear(self):
        self._scheduler.remove_all_jobs()
        self._scheduler.shutdown()

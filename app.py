# -*- coding: utf-8 -*-

"""
Camera device web interface

MIT License

Copyright (c) 2017 Roman Mindlin

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
"""

import os
from random import randint
from subprocess import run, Popen, PIPE
import time
from time import sleep
from threading import Thread, Lock
from string import Template

import numpy as np
import picamera
from picamera.array import PiRGBArray

import cv2

from flask import Flask, make_response, request, render_template, jsonify, send_file, send_from_directory, Response

import cam
import const
import scheduler
import uploader
import logger

logger = logger.get_logger('app.py')
camera_lock = Lock()
panorama_lock = Lock()
basedir = os.path.abspath(os.path.dirname(__file__))

app = Flask(__name__)
cam_obj = cam.Cam(logger=app.logger, debug=app.config['DEBUG'])
uploader = uploader.Uploader(enable=cam_obj.ftp)

rnd = randint(1e2,1e3)

"""
 ****************** VIEWS ******************
"""


@app.route('/', methods=['GET', 'POST'])
def index():
    ' Home page '
    if request.method == 'GET':
        if request.cookies.get('token') == str(rnd):
            return render_template('index.html', rnd=rnd)
        else:
            return render_template('login.html')
    elif request.method == 'POST':
        # check credentials
        if 'secret' in request.form:
            if request.form.get('secret') == const.SECRET:
                response = make_response(render_template('index.html', rnd=rnd))
                response.set_cookie('token', value=str(rnd))
                return response
        return render_template('login.html')


@app.route('/video_feed')
def video_feed():
    ' Implements camera preview video stream '

    def get_frame():
        ' Yields one frame '
        cam_names = ['L', 'D', 'C', 'R']
        with camera_lock:
            if cam_obj.dp:
                rawCapture = PiRGBArray(cam_obj.camera, size=(640, 480))
                cam_obj.camera.annotate_text = ''
                cam_obj.camera.capture(rawCapture, 'bgr', resize=(640, 480))
                frame = rawCapture.array
                K, D, Knew, x, y, w, h = cam_obj.get_video_params()
                frame = cv2.fisheye.undistortImage(frame, K, D=D, Knew=Knew)
                frame = cv2.rectangle(frame, (255,55), (385,25), (0,0,0), cv2.FILLED)
                frame = cv2.putText(frame, 'CAM #' + cam_names[cam_obj.current_cam - 1], (260, 50),
                                    cv2.FONT_HERSHEY_DUPLEX, 1, (255,255,255))
                ret, frame = cv2.imencode('.jpg', frame)
            else:
                frame = np.empty((640, 480, 3), dtype=np.uint8)
                cam_obj.camera.annotate_background = picamera.Color('black')
                cam_obj.camera.annotate_text = 'CAM #' + cam_names[cam_obj.current_cam - 1]
                cam_obj.camera.capture(frame, 'jpeg', resize=(640, 480))
        return frame.tobytes()

    # logger.debug('Sending frame')
    if app.config['DEBUG']:
        return send_file('video_feed.jpg', mimetype='image/jpeg')
    else:
        return Response(get_frame(), mimetype='image/jpeg')


"""
 ****************** REST API ******************
"""


@app.route('/api/select/<int:num>', methods=['GET'])
def select(num):
    ' Select current camera '
    if 1 <= num <= 4:
        cam_obj.select(num)
        logger.info('Camera {} selected'.format(cam_obj.current_cam))
        return "<h1>Ok</h1>", 200
    else:
        return "<h1>Error</h1>", 400


@app.route('/api/params', methods=['GET'])
def params():
    ' Sends camera object parameters '
    return jsonify(**cam_obj.get_params()), 200


@app.route('/api/params_modified', methods=['GET'])
def params_modified():
    ' Returns was parameters modified or not '
    return str(cam_obj.is_modified), 200


@app.route('/api/list_presets', methods=['GET'])
def list_presets():
    ' List presets '
    presets = []
    for filename in os.listdir(const.LOCAL_PRESETS_PATH):
        if os.path.isfile(os.path.join(const.LOCAL_PRESETS_PATH, filename)) and filename.endswith('pkl'):
            if filename == const.LOCAL_PRESET:
                filename = '+' + filename
            presets.append(filename)
    return jsonify(presets), 200


@app.route('/api/load_preset/<string:filename>', methods=['GET'])
def load_preset(filename):
    ' Loads preset from file '
    try:
        if os.path.isfile(os.path.join(const.LOCAL_PRESETS_PATH, filename)) and filename.endswith('pkl'):
            cam_obj.load_params(os.path.join(const.LOCAL_PRESETS_PATH, filename))
            const.LOCAL_PRESET = filename
            save_const()
            logger.info('Preset {} loaded'.format(filename))
            return "<h1>Ok</h1>", 200
        else:
            return "<h1>Error</h1>", 400
    except:
        return "<h1>Error</h1>", 400


@app.route('/api/save_preset', methods=['POST'])
def save_preset():
    ' Saves preset from file '
    try:
        val = request.form['val']
        if val == str(rnd):
            if not os.path.isdir(const.LOCAL_PRESETS_PATH):
                os.makedirs(const.LOCAL_PRESETS_PATH)
            filename = request.form['name']
            if not filename.endswith('.pkl'):
                filename += '.pkl'
            cam_obj.save_params(os.path.join(const.LOCAL_PRESETS_PATH, filename))
            const.LOCAL_PRESET = filename
            save_const()
            logger.info('Preset {} saved'.format(filename))
            return "<h1>Ok</h1>", 200
        else:
            return "<h1>Error</h1>", 400
    except:
        return "<h1>Error</h1>", 400


def get_const(prefix=None):
    ' Compose settings in a dict '
    result = {}
    constants = [x for x in dir(const) if x.find('__')]
    for one in constants:
        if one.find('PASS') > 0 or one.startswith('LOCAL') or one == 'SECRET':
            continue
        if prefix is not None:
            if not one.startswith(prefix):
                continue
        result[one] = getattr(const, one)
    return result

def save_const():
    ' Saves constants file '
    constants = [x for x in dir(const) if x.find('__')]
    with open(const.__file__, 'w') as f:
        f.write('# -*- coding: utf-8 -*-\n\n')
        for one in constants:
            attr = getattr(const, one)
            if isinstance(attr, str):
                attr = '\'' + attr + '\''
            else:
                attr = str(attr)
            f.write(one + ' = ' + attr + '\n')

@app.route('/api/settings', methods=['GET', 'POST'])
def settings():
    ' Settings modal support '
    if request.method == 'POST':
        try:
            val = request.form['val']
            if val == str(rnd):
                # save FTP settings
                need_save = False
                if len(request.form['ftpHost']):
                    const.FTP_HOST = request.form['ftpHost']
                    need_save = True
                if len(request.form['ftpPath']):
                    const.FTP_PATH = request.form['ftpPath']
                    need_save = True
                if len(request.form['ftpUser']):
                    const.FTP_USER = request.form['ftpUser']
                    need_save = True
                const.FTP_FILES = request.form['ftpFiles'] == 'true'
                const.FTP_PASS = request.form['ftpPass']
                if len(request.form['secret']):
                    const.SECRET = request.form['secret']
                    need_save = True

                if need_save:
                    save_const()
                    logger.debug('Settings saved')

                # save PTO template
                if len(request.form['template']):
                    with open(const.LOCAL_PTO_TEMPLATE, 'w') as f:
                        f.write(request.form['template'])
                    logger.debug('Template saved')

                return "<h1>Ok</h1>", 200
            else:
                return "<h1>Error</h1>", 400
        except:
            return "<h1>Error</h1>", 400
    else:
        settings = get_const()
        if os.path.isfile(const.LOCAL_PTO_TEMPLATE):
            with open(const.LOCAL_PTO_TEMPLATE, 'r') as t:
                settings['template'] = t.read()
        return jsonify(**settings)


@app.route('/api/schedule', methods=['GET', 'POST'])
def schedule():
    ' Schedule modal support '
    if request.method == 'POST':
        try:
            val = request.form['val']
            if val == str(rnd):
                # save schedule
                need_save = False
                if len(request.form['fromHour']):
                    const.SCHEDULER_FROM_HOUR = request.form['fromHour']
                    need_save = True
                if len(request.form['toHour']):
                    const.SCHEDULER_TO_HOUR = request.form['toHour']
                    need_save = True
                if len(request.form['interval']):
                    const.SCHEDULER_INTERVAL = request.form['interval']
                    need_save = True
                if len(request.form['interval2']):
                    const.SCHEDULER_INTERVAL2 = request.form['interval2']
                    need_save = True

                if need_save:
                    save_const()
                    logger.debug('Schedule saved')
                    f4starter.update()

                return "<h1>Ok</h1>", 200
            else:
                return "<h1>Error</h1>", 400
        except:
            return "<h1>Error</h1>", 400
    else:
        settings = get_const('SCHEDULER')
        return jsonify(**settings)


@app.route('/api/network', methods=['GET', 'POST'])
def network():
    ' Network modal support '
    if request.method == 'POST':
        try:
            val = request.form['val']
            if val == str(rnd):
                # save schedule
                const.NETWORK_TYPE = request.form['networkType']
                if const.NETWORK_TYPE == 'wired':
                    const.NETWORK_ADDRESS = request.form['address']
                    const.NETWORK_MASK = request.form['netmask']
                    const.NETWORK_GATEWAY = request.form['gateway']
                    const.NETWORK_DNS = request.form['dns']
                elif const.NETWORK_TYPE == 'wifi':
                    const.NETWORK_SSID = request.form['ssid']
                    const.NETWORK_WIFIPASS = request.form['wifipass']
                else:
                    return "<h1>Error</h1>", 400

                save_const()
                with open(os.path.join(const.LOCAL_CONFIG_PATH, 'need_reconfigure'), 'a'):
                    os.utime(os.path.join(const.LOCAL_CONFIG_PATH, 'need_reconfigure'), None)
                logger.debug('Network settings saved')

                return "<h1>Ok</h1>", 200
            else:
                return "<h1>Error</h1>", 400
        except:
            return "<h1>Error</h1>", 400
    else:
        settings = get_const('NETWORK')
        return jsonify(**settings)


@app.route('/api/iso', methods=['POST'])
def set_iso():
    ' Set camera ISO '
    try:
        val = request.form['val']
        if val in ['0', '100', '200', '400', '800']:
            cam_obj.iso = int(val)
            logger.debug('ISO = {}'.format(cam_obj.iso))
            return str(cam_obj.iso), 200
        else:
            return "<h1>Error</h1>", 400
    except:
        return "<h1>Error</h1>", 400


@app.route('/api/exp', methods=['POST'])
def set_exp():
    ' Set camera EXP '
    try:
        val = request.form['val']
        if -25 <= int(val) <= 25:
            cam_obj.exp = int(val)
            logger.debug('EXP = {}'.format(cam_obj.exp))
            return str(cam_obj.exp), 200
        else:
            return "<h1>Error</h1>", 400
    except:
        return "<h1>Error</h1>", 400


@app.route('/api/contr', methods=['POST'])
def set_contr():
    ' Set camera contrast '
    try:
        val = request.form['val']
        if -25 <= int(val) <= 25:
            cam_obj.contr = int(val)
            logger.debug('Contr = {}'.format(cam_obj.contr))
            return str(cam_obj.contr), 200
        else:
            return "<h1>Error</h1>", 400
    except:
        return "<h1>Error</h1>", 400


@app.route('/api/drc', methods=['POST'])
def set_drc():
    ' Set DRC '
    try:
        val = request.form['val']
        if val in ['high', 'low', 'off']:
            cam_obj.drc = val
            logger.debug('DRC = {}'.format(cam_obj.drc))
            return str(cam_obj.drc), 200
        else:
            return "<h1>Error</h1>", 400
    except:
        return "<h1>Error</h1>", 400


@app.route('/api/splus', methods=['GET'])
def shutter_plus():
    ' Increase shutter '
    try:
        cam_obj.shutter *= 2
        logger.debug('Shutter inc = {}'.format(cam_obj.shutter))
        return "1/" + str(int(1e6 / cam_obj.shutter)), 200
    except:
        return "<h1>Error</h1>", 400


@app.route('/api/sminus', methods=['GET'])
def shutter_minus():
    ' Decrease shutter '
    try:
        cam_obj.shutter //= 2
        logger.debug('Shutter dec = {}'.format(cam_obj.shutter))
        return "1/" + str(int(1e6 / cam_obj.shutter)), 200
    except:
        return "<h1>Error</h1>", 400


@app.route('/api/sauto', methods=['GET'])
def shutter_auto():
    ' Automatic shutter '
    try:
        cam_obj.shutter = 0
        logger.debug('Shutter auto = {}'.format(cam_obj.shutter))
        return "1/" + str(int(1e6 / cam_obj.shutter)), 200
    except:
        return "<h1>Error</h1>", 400


@app.route('/api/mode', methods=['POST'])
def set_mode():
    ' Mode setting '
    try:
        val = request.form['val']
        if val in ['P', 'auto', 'off']:
            cam_obj.mode = val
            logger.debug('Mode = {}'.format(cam_obj.mode))
            return str(cam_obj.mode), 200
        else:
            return "<h1>Error</h1>", 400
    except:
        return "<h1>Error</h1>", 400


@app.route('/api/res', methods=['POST'])
def set_res():
    ' Resolution setting '
    try:
        val = request.form['val']
        if val in ['1', '2', '3', '4']:
            cam_obj.resolution = val
            logger.debug('Res = {}'.format(cam_obj.resolution))
            return str(cam_obj.resolution), 200
        else:
            return "<h1>Error</h1>", 400
    except:
        return "<h1>Error</h1>", 400


@app.route('/api/wb', methods=['POST'])
def set_wb():
    ' WB mode setting '
    try:
        val = request.form['val']
        if val in ['auto', 'sunlight', 'cloudy', 'tungsten', 'shade', 'off']:
            cam_obj.wb = val
            logger.debug('WB = {}'.format(cam_obj.wb))
            return str(cam_obj.wb), 200
        else:
            return "<h1>Error</h1>", 400
    except:
        return "<h1>Error</h1>", 400


@app.route('/api/wb_red', methods=['POST'])
def set_wb_red():
    ' WB red value setting '
    try:
        val = request.form['val']
        if 0 <= int(val) <= 80:
            cam_obj.wb_red = val
            logger.debug('WB red = {}'.format(cam_obj.wb_red))
            return str(cam_obj.wb_red), 200
        else:
            return "<h1>Error</h1>", 400
    except:
        return "<h1>Error</h1>", 400


@app.route('/api/wb_blue', methods=['POST'])
def set_wb_blue():
    ' WB blue value setting '
    try:
        val = request.form['val']
        if 0 <= int(val) <= 80:
            cam_obj.wb_blue = val
            logger.debug('WB blue = {}'.format(cam_obj.wb_blue))
            return str(cam_obj.wb_blue), 200
        else:
            return "<h1>Error</h1>", 400
    except:
        return "<h1>Error</h1>", 400


@app.route('/api/zoom', methods=['POST'])
def zoom():
    ' Zoom value setting '
    try:
        val = request.form['val']
        if 20 <= int(val) <= 100:
            cam_obj.zoom = val
            logger.debug('zoom = {}'.format(cam_obj.zoom))
            return str(cam_obj.zoom), 200
        else:
            return "<h1>Error</h1>", 400
    except:
        return "<h1>Error</h1>", 400


@app.route('/api/tilt', methods=['POST'])
def tilt():
    ' Tilt value setting '
    try:
        val = request.form['val']
        if -50 <= int(val) <= 50:
            cam_obj.tilt = val
            logger.debug('tilt = {}'.format(cam_obj.tilt))
            return str(cam_obj.tilt), 200
        else:
            return "<h1>Error</h1>", 400
    except:
        return "<h1>Error</h1>", 400


@app.route('/api/ftp', methods=['POST'])
def ftp():
    ' FTP value setting '
    try:
        val = request.form['val']
        if val in ['true', 'false']:
            cam_obj.ftp = val
            uploader.enable(val == 'true')
            logger.debug('ftp = {}'.format(cam_obj.ftp))
            return str(cam_obj.ftp), 200
        else:
            return "<h1>Error</h1>", 400
    except:
        return "<h1>Error</h1>", 400


@app.route('/api/hg', methods=['POST'])
def hg():
    ' DP value setting '
    try:
        val = request.form['val']
        if val in ['true', 'false']:
            cam_obj.hg = val
            logger.debug('hugin = {}'.format(cam_obj.hg))
            return str(cam_obj.dp), 200
        else:
            return "<h1>Error</h1>", 400
    except:
        return "<h1>Error</h1>", 400


@app.route('/api/dp', methods=['POST'])
def dp():
    ' DP value setting '
    try:
        val = request.form['val']
        if val in ['true', 'false']:
            cam_obj.dp = val
            logger.debug('DP = {}'.format(cam_obj.dp))
            return str(cam_obj.dp), 200
        else:
            return "<h1>Error</h1>", 400
    except:
        return "<h1>Error</h1>", 400


@app.route('/api/f1', methods=['GET'])
def f1():
    ' Make one picture from current camera '
    res_list = [(2592, 1944), (1620, 1232), (1296, 972), (640, 480)]
    cam_obj.camera.resolution = res_list[cam_obj.resolution - 1]

    try:
        if not os.path.isdir(const.LOCAL_PICTURES_PATH):
            os.makedirs(const.LOCAL_PICTURES_PATH)
        with camera_lock:
            cam_obj.camera.annotate_text = ''
            cam_obj.camera.exif_tags['IFD0.Artist'] = "RWPBB"
            cam_obj.camera.exif_tags['EXIF.FocalLength'] = const.LOCAL_EXIF_FF
            filename = str(cam_obj.session_num) + "c" + str(cam_obj.current_cam) + "pi%03d.jpg" % cam_obj.counter
            cam_obj.camera.capture(os.path.join(const.LOCAL_PICTURES_PATH, filename))
            logger.debug('Picture {} saved'.format(filename))

    except Exception as e:
        logger.error('File creation error {}'.format(filename))
        cam_obj.camera.resolution = (640, 480)
        return 'filesystem error', 400

    cam_obj.camera.resolution = (640, 480)
    return filename, 200


@app.route('/api/f4', methods=['GET'])
def f4():
    ' Make panoramic picture '
    with panorama_lock:
        res_list = [(2592, 1944), (1620, 1232), (1296, 972), (640, 480)]

        try:
            # Prepare directory
            if not os.path.isdir(const.LOCAL_PICTURES_PATH):
                os.makedirs(const.LOCAL_PICTURES_PATH)

            with camera_lock:
                # Prepare variables
                temp_cam = cam_obj.current_cam
                counter = cam_obj.counter
                picture_num = 10 * cam_obj.session_num + counter

                # Prepare camera
                cam_obj.camera.resolution = res_list[cam_obj.resolution - 1]
                cam_obj.camera.annotate_text = ''
                cam_obj.camera.exif_tags['IFD0.Artist'] = "RWPBB"
                cam_obj.camera.exif_tags['EXIF.FocalLength'] = const.LOCAL_EXIF_FF

                # Make 4 pictures
                start_time = time.time()
                for cam_num in range(1, 5):
                    cam_obj.select(cam_num)
                    cam_obj.camera.capture(os.path.join(const.LOCAL_PICTURES_PATH,  "c" + str(cam_obj.current_cam) + "pi%04d.jpg" % picture_num))
                elapsed_time = time.time() - start_time
                logger.debug('Panorama source saved in {:.2f} seconds'.format(elapsed_time))

                # Restore camera mode
                cam_obj.select(temp_cam)
                cam_obj.camera.resolution = (640, 480)

            # Make project file
            pto_name = str(cam_obj.session_num) + 'k%03d.pto' % picture_num
            pano_name = str(cam_obj.session_num) + 'k%03d.jpg' % picture_num
            files = dict([('file' + str(i), 'c' + str(i) + 'pi%04d.jpg' % picture_num) for i in range(1, 5)])
            with open(os.path.join(const.LOCAL_PICTURES_PATH, pto_name), 'w') as f, open(const.LOCAL_PTO_TEMPLATE, 'r') as t:
                pto_file = Template(t.read()).substitute(**files)
                f.write(pto_file)
            logger.debug('Panorama {} saved'.format(pto_name))

            if cam_obj.hg:
                p = Popen(["/usr/bin/hugin_executor", "-s", "-p"+pano_name, pto_name],
                          cwd=const.LOCAL_PICTURES_PATH, stdout=PIPE, stderr=PIPE)

            # Send results via FTP
            if cam_obj.ftp:
                if const.FTP_FILES:
                    files_to_send = ['c' + str(num) + 'pi%04d.jpg' % picture_num for num in range(1, 5)]
                    files_to_send.append(str(cam_obj.session_num) + 'k%03d.pto' % counter)
                else:
                    files_to_send = []

                if cam_obj.hg:
                    files_to_send.append(pano_name)

                uploader.append(files_to_send)
                logger.debug('FTP transmit: {}'.format(files_to_send))

        except Exception as e:
            # Exception during panorama creation
            logger.error('Panorama creation error: {}'.format(pto_name))
            cam_obj.select(temp_cam)
            cam_obj.camera.resolution = (640, 480)
            return 'filesystem error', 400

    return pano_name, 200


@app.route('/api/lc', methods=['GET'])
def lc():
    ' Calculate LC '
    frame = np.empty((640, 480, 3), dtype=np.uint8)
    cam_obj.camera.capture(frame, 'bgr')
    gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
    fm = cv2.Laplacian(gray, cv2.CV_64F).var()
    logger.debug('LC = {}'.format(int(fm)))
    return str(int(fm)), 200


"""
 ****************** UTILS ******************
"""


@app.route('/scripts/<string:name>', methods=['GET'])
def js(name):
    ' Returns static JS file '
    if name == str(rnd):
        return render_template('all.js', rnd=rnd)
    elif os.path.isfile(os.path.join(basedir, *['js', name])):
        return send_from_directory('js',name)
    else:
        return '<h1>Not found</h1>', 404


@app.route('/css/<string:name>', methods=['GET'])
def css(name):
    ' Returns static CSS file '
    if os.path.isfile(os.path.join(basedir, *['css', name])):
        return send_from_directory('css',name)
    else:
        return '<h1>Not found</h1>', 404


@app.route('/favicon.ico', methods=['GET'])
def favicon():
    ' Returns favicon '
    favicon = os.path.join(basedir, *['templates', 'favicon.ico'])
    if os.path.isfile(favicon):
        return send_file(favicon)
    else:
        return '<h1>Not found</h1>', 404


def switchoff_thread():
    ' Waits 2 secs to let app respond and shutdowns OS after that '
    sleep(2)
    run("sudo poweroff", shell=True)

@app.route('/api/switchoff', methods=['POST'])
def switchoff():
    ' Switch OFF device '
    try:
        val = request.form['val']
        if val == str(rnd):
            f4starter.clear()
            uploader.close()
            t = Thread(target=switchoff_thread).start() #Switch OFF
            logger.info('Switch off command')
            return '<h1>OK</h1>', 200
    except:
        pass
    return "<h1>Error</h1>", 400


def reboot_thread():
    ' Waits 2 secs to let app respond and shutdowns OS after that '
    sleep(2)
    run("sudo reboot", shell=True)

@app.route('/api/restart', methods=['POST'])
def reboot():
    ' Reboot device '
    try:
        val = request.form['val']
        if val == str(rnd):
            f4starter.clear()
            uploader.close()
            t = Thread(target=reboot_thread).start() #Reboot
            logger.info('Reboot command')
            return '<h1>OK</h1>', 200
    except:
        pass
    return "<h1>Error</h1>", 400


def shutdown_server():
    ' Exits app '
    func = request.environ.get('werkzeug.server.shutdown')
    if func is None:
        raise RuntimeError('Not running with the Werkzeug Server')
    func()

@app.route('/api/shutdown', methods=['POST'])
def shutdown():
    ' Switch OFF device '
    try:
        val = request.form['val']
        if val == str(rnd):
            f4starter.clear()
            uploader.close()
            logger.info('Exit command')
            shutdown_server()  # Exit
    except:
        pass
    return "<h1>Error</h1>", 400


"""
 ****************** ERRORS ******************
"""


@app.errorhandler(404)
def page_not_found(e):
    '404 error handler'
    return "<h1>Error 404</h1>", 404


# @app.errorhandler(Exception)
# def all_exception_handler(error):
#     app.logger.info('Exiting...')
#     cam_obj.cleanup()

f4starter = scheduler.F4Scheduler(f4)
f4starter.set()

if __name__ == "__main__":
    app.run(debug=False, host='0.0.0.0', port=8888)

# -*- coding: utf-8 -*-

"""
MIT License

Copyright (c) 2017 Roman Mindlin

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
"""

from app import app, rnd
import hashlib
import unittest


from coverage import coverage

cov = coverage(branch=True, omit=['/home/pi/.virtualenvs/pi3/lib/python3.5/site-packages/*', 'tests.py'])
cov.start()


class BasicTests(unittest.TestCase):

    def setUp(self):
        app.config['TESTING'] = True
        self.app = app.test_client()

        self.assertEqual(app.debug, False)
        pass

    def tearDown(self):
        pass

    def login(self, secret):
        m = hashlib.sha256()
        m.update(secret.encode())
        return self.app.post('/', data=dict(secret=m.hexdigest()), follow_redirects=True)

    def test_index(self):
        response = self.app.get('/', content_type='html/text', follow_redirects=True)
        self.assertEqual(response.status_code, 200)

    def test_login(self):
        response = self.login('111')
        self.assertIn(b'Enter password', response.data)

        response = self.login('123')
        self.assertIn(b'2592x1944', response.data)

    def test_video_feed(self):
        response = self.app.get('/video_feed')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.content_type, 'image/jpeg')


class APITests(unittest.TestCase):

    def setUp(self):
        app.config['TESTING'] = True
        self.app = app.test_client()

        self.assertEqual(app.debug, False)
        self.rnd = rnd

    def tearDown(self):
        pass

    def login(self, secret):
        m = hashlib.sha256()
        m.update(secret.encode())
        return self.app.post('/', data=dict(secret=m.hexdigest()), follow_redirects=True)

    def test_select(self):
        response = self.app.get('/api/select/99', content_type='html/text')
        self.assertEqual(response.status_code, 400)

        response = self.app.get('/api/select/1', content_type='html/text')
        self.assertEqual(response.status_code, 200)

    def test_params(self):
        response = self.app.get('/api/params')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.content_type, 'application/json')

    def test_params_modified(self):
        response = self.app.get('/api/params_modified')
        self.assertEqual(response.status_code, 200)
        self.assertTrue(response.data == b'True' or response.data == b'False')

    def test_list_presets(self):
        response = self.app.get('/api/list_presets')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.content_type, 'application/json')

    def test_load_preset(self):
        response = self.app.get('/api/load_preset')
        self.assertEqual(response.status_code, 404)

        response = self.app.get('/api/load_preset/test9753')
        self.assertEqual(response.status_code, 400)

    def test_save_preset(self):
        response = self.app.post('/api/save_preset', data=dict(val=0, name='test'), follow_redirects=True)
        self.assertEqual(response.status_code, 400)

    def test_settings(self):
        response = self.app.post('/api/settings', data=dict(val=0,
                                                            ftpHost='test',
                                                            ftpPath='test',
                                                            ftpUser='test',
                                                            ftpFiles='true',
                                                            ftpPass='test',
                                                            secret='',
                                                            template=''), follow_redirects=True)
        self.assertEqual(response.status_code, 400)

        response = self.app.get('/api/settings')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.content_type, 'application/json')

    def test_schedule(self):
        response = self.app.post('/api/schedule', data=dict(val=0,
                                                            fromHour='0',
                                                            toHour='23',
                                                            interval='10',
                                                            interval2='30'), follow_redirects=True)
        self.assertEqual(response.status_code, 400)

        response = self.app.get('/api/schedule')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.content_type, 'application/json')

    def test_iso(self):
        response = self.app.post('/api/iso', data=dict(val='100'), follow_redirects=True)
        self.assertIn(b'100', response.data)
        self.assertEqual(response.status_code, 200)

        response = self.app.post('/api/iso', data=dict(val='900'), follow_redirects=True)
        self.assertEqual(response.status_code, 400)

    def test_exp(self):
        response = self.app.post('/api/exp', data=dict(val='0'), follow_redirects=True)
        self.assertIn(b'0', response.data)
        self.assertEqual(response.status_code, 200)

        response = self.app.post('/api/exp', data=dict(val='-90'), follow_redirects=True)
        self.assertEqual(response.status_code, 400)

    def test_contr(self):
        response = self.app.post('/api/contr', data=dict(val='0'), follow_redirects=True)
        self.assertIn(b'0', response.data)
        self.assertEqual(response.status_code, 200)

        response = self.app.post('/api/contr', data=dict(val='-90'), follow_redirects=True)
        self.assertEqual(response.status_code, 400)

    def test_drc(self):
        response = self.app.post('/api/drc', data=dict(val='off'), follow_redirects=True)
        self.assertIn(b'off', response.data)
        self.assertEqual(response.status_code, 200)

        response = self.app.post('/api/drc', data=dict(val='0'), follow_redirects=True)
        self.assertEqual(response.status_code, 400)

    def test_shutter(self):
        response = self.app.get('/api/sauto')
        self.assertIn(b'1/', response.data)
        self.assertEqual(response.status_code, 200)

        response = self.app.get('/api/splus')
        self.assertIn(b'1/', response.data)
        self.assertEqual(response.status_code, 200)

        response = self.app.get('/api/sminus')
        self.assertIn(b'1/', response.data)
        self.assertEqual(response.status_code, 200)

    def test_mode(self):
        response = self.app.post('/api/mode', data=dict(val='off'), follow_redirects=True)
        self.assertIn(b'off', response.data)
        self.assertEqual(response.status_code, 200)

        response = self.app.post('/api/mode', data=dict(val='0'), follow_redirects=True)
        self.assertEqual(response.status_code, 400)

    def test_res(self):
        response = self.app.post('/api/res', data=dict(val='1'), follow_redirects=True)
        self.assertIn(b'1', response.data)
        self.assertEqual(response.status_code, 200)

        response = self.app.post('/api/res', data=dict(val='0'), follow_redirects=True)
        self.assertEqual(response.status_code, 400)

    def test_wb(self):
        response = self.app.post('/api/wb', data=dict(val='auto'), follow_redirects=True)
        self.assertIn(b'auto', response.data)
        self.assertEqual(response.status_code, 200)

        response = self.app.post('/api/wb', data=dict(val='0'), follow_redirects=True)
        self.assertEqual(response.status_code, 400)

        response = self.app.post('/api/wb_red', data=dict(val='10'), follow_redirects=True)
        self.assertIn(b'10', response.data)
        self.assertEqual(response.status_code, 200)

        response = self.app.post('/api/wb_red', data=dict(val='-1'), follow_redirects=True)
        self.assertEqual(response.status_code, 400)

        response = self.app.post('/api/wb_blue', data=dict(val='10'), follow_redirects=True)
        self.assertIn(b'10', response.data)
        self.assertEqual(response.status_code, 200)

        response = self.app.post('/api/wb_blue', data=dict(val='-1'), follow_redirects=True)
        self.assertEqual(response.status_code, 400)

    def test_zoom(self):
        response = self.app.post('/api/zoom', data=dict(val='100'), follow_redirects=True)
        self.assertIn(b'100', response.data)
        self.assertEqual(response.status_code, 200)

        response = self.app.post('/api/zoom', data=dict(val='0'), follow_redirects=True)
        self.assertEqual(response.status_code, 400)

    def test_tilt(self):
        response = self.app.post('/api/tilt', data=dict(val='0'), follow_redirects=True)
        self.assertIn(b'0', response.data)
        self.assertEqual(response.status_code, 200)

        response = self.app.post('/api/tilt', data=dict(val='-100'), follow_redirects=True)
        self.assertEqual(response.status_code, 400)

    def test_ftp(self):
        response = self.app.post('/api/ftp', data=dict(val='true'), follow_redirects=True)
        self.assertIn(b'True', response.data)
        self.assertEqual(response.status_code, 200)

        response = self.app.post('/api/ftp', data=dict(val='0'), follow_redirects=True)
        self.assertEqual(response.status_code, 400)

    def test_hg(self):
        response = self.app.post('/api/hg', data=dict(val='true'), follow_redirects=True)
        self.assertIn(b'True', response.data)
        self.assertEqual(response.status_code, 200)

        response = self.app.post('/api/hg', data=dict(val='0'), follow_redirects=True)
        self.assertEqual(response.status_code, 400)

    def test_dp(self):
        response = self.app.post('/api/dp', data=dict(val='true'), follow_redirects=True)
        self.assertIn(b'True', response.data)
        self.assertEqual(response.status_code, 200)

        response = self.app.post('/api/dp', data=dict(val='0'), follow_redirects=True)
        self.assertEqual(response.status_code, 400)

    def test_lc(self):
        response = self.app.get('/api/lc')
        self.assertEqual(response.status_code, 200)

    def test_js(self):
        response = self.app.get('/scripts/' + str(rnd))
        self.assertEqual(response.status_code, 200)
        self.assertIn('text/html', response.content_type)


if __name__ == '__main__':
    try:
        unittest.main()
    except:
        pass
    cov.stop()
    cov.save()
    print("\n\nCoverage Report:\n")
    cov.report()
    cov.erase()

# -*- coding: utf-8 -*-

"""
Camera device web interface

MIT License

Copyright (c) 2017 Roman Mindlin

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
"""

from random import randint

import numpy as np
import picamera
from picamera.array import PiRGBArray

import cv2

from flask import Blueprint, request, render_template, send_file, make_response, Response
from flask import current_app as app

import const

rnd = randint(1e2,1e3)

"""
 ****************** VIEWS ******************
"""


views = Blueprint('views', __name__)

@views.route('/', methods=['GET', 'POST'])
def index():
    ' Home page '
    if request.method == 'GET':
        if request.cookies.get('token') == str(rnd):
            return render_template('index.html', rnd=rnd)
        else:
            return render_template('login.html')
    elif request.method == 'POST':
        # check credentials
        if 'secret' in request.form:
            if request.form.get('secret') == const.SECRET:
                response = make_response(render_template('index.html', rnd=rnd))
                response.set_cookie('token', value=str(rnd))
                return response
        return render_template('login.html')


@views.route('/video_feed')
def video_feed():
    ' Implements camera preview video stream '

    def get_frame():
        ' Yields one frame '
        cam_names = ['L', 'D', 'C', 'R']
        with camera_lock:
            if cam_obj.dp:
                rawCapture = PiRGBArray(cam_obj.camera, size=(640, 480))
                cam_obj.camera.annotate_text = ''
                cam_obj.camera.capture(rawCapture, 'bgr', resize=(640, 480))
                frame = rawCapture.array
                K, D, Knew, x, y, w, h = cam_obj.get_video_params()
                frame = cv2.fisheye.undistortImage(frame, K, D=D, Knew=Knew)
                frame = cv2.rectangle(frame, (255,55), (385,25), (0,0,0), cv2.FILLED)
                frame = cv2.putText(frame, 'CAM #' + cam_names[cam_obj.current_cam - 1], (260, 50),
                                    cv2.FONT_HERSHEY_DUPLEX, 1, (255,255,255))
                ret, frame = cv2.imencode('.jpg', frame)
            else:
                frame = np.empty((640, 480, 3), dtype=np.uint8)
                cam_obj.camera.annotate_background = picamera.Color('black')
                cam_obj.camera.annotate_text = 'CAM #' + cam_names[cam_obj.current_cam - 1]
                cam_obj.camera.capture(frame, 'jpeg', resize=(640, 480))
        return frame.tobytes()

    # logger.debug('Sending frame')
    if app.config['DEBUG']:
        return send_file('video_feed.jpg', mimetype='image/jpeg')
    else:
        return Response(get_frame(), mimetype='image/jpeg')
